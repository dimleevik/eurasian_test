const { Sequelize } = require("sequelize");
const dbConfig = require("../../../common/config/dbConfig");

const connection = new Sequelize(dbConfig);

module.exports = connection;
