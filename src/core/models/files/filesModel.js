const Sequelize = require("sequelize");
const connection = require("../db/dbConnection");
const UserModel = require("../users/userModel");

const FilesModel = connection.define(
	"Files",
	{
		id: {
			type: Sequelize.INTEGER,
			primaryKey: true,
			autoIncrement: true,
		},
		name: {
			type: Sequelize.STRING,
		},
		userId: {
			type: Sequelize.INTEGER,
			field: "user_id",
			references: {
				model: UserModel,
				key: "id",
			},
		},
	},
	{ tableName: "file" }
);

module.exports = FilesModel;
