const Sequelize = require("sequelize");
const bcrypt = require("bcrypt");
const connection = require("../db/dbConnection");

const UserModel = connection.define(
	"User",
	{
		id: {
			type: Sequelize.INTEGER,
			primaryKey: true,
			autoIncrement: true,
		},
		email: {
			type: Sequelize.STRING,
			unique: true,
		},
		password: {
			type: Sequelize.STRING,
		},
	},
	{
		hooks: {
			beforeCreate: async (user) => {
				user.password = await bcrypt.hash(user.password, 6);
			},
		},
		tableName: "user",
	}
);

module.exports = UserModel;
