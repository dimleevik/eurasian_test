const { Router } = require("express");
const FileRouter = require("./file/fileRoutes");
const AuthRoutes = require("./auth/authRoutes");

const routes = Router();

routes.use("/files", FileRouter);
routes.use("/auth", AuthRoutes);

module.exports = routes;
