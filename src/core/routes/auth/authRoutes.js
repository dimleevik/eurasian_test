const { Router } = require("express");

const AuthService = require("../../services/auth/authService");
const { body, validationResult } = require("express-validator");

const router = Router();

router.post(
	"/login",
	body("email").isEmail(),
	body("password").isLength({ min: 6 }),
	async (req, res) => {
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() });
		}
		const { email, password } = req.body;
		AuthService.login({ email, password }, res);
	}
);

router.post(
	"/register",
	body("email").isEmail(),
	body("password").isLength({ min: 6 }),
	async (req, res) => {
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() });
		}
		const { email, password } = req.body;
		AuthService.register({ email, password }, res);
	}
);

module.exports = router;
