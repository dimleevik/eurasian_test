const { Router } = require("express");
const multer = require("multer");
const { isAuthorized } = require("../../middlewares/auth/authMiddleware");
const FilesService = require("../../services/files/filesService");
const upload = multer();

const router = Router();

router.get("/", isAuthorized, FilesService.list);
router.get("/:id", isAuthorized, FilesService.get);
router.post("/", isAuthorized, upload.array("images"), FilesService.create);

module.exports = router;
