const jwt = require("jsonwebtoken");

class JwtService {
	static sign(data) {
		return jwt.sign(data, "secret", { expiresIn: 600 });
	}

	static verify(token) {
		return jwt.verify(token, "secret");
	}
}

module.exports = JwtService;
