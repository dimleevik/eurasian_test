const fs = require("fs");
const fsPromises = require("fs/promises");
const FilesModel = require("../../models/files/filesModel");

class FilesService {
	static async list({ user }, res) {
		const files = await FilesModel.findAndCountAll({
			where: { userId: user.id },
			raw: true,
		});
		res.send({
			items: files.rows,
			count: files.count,
		});
	}

	static async create(req, res) {
		if (!fs.existsSync(`dist/${req.user.id}`)) {
			fs.mkdirSync(`dist/${req.user.id}`, { recursive: true });
		}
		const result = await Promise.allSettled(
			req.files.map(async (file) => {
				await fsPromises.writeFile(
					`dist/${req.user.id}/${file.originalname}`,
					file.buffer
				);
				return FilesModel.create(
					{
						userId: req.user.id,
						name: file.originalname,
					},
					{ raw: true }
				);
			})
		);
		res.send(result);
	}

	static async get(req, res) {
		const file = await FilesModel.findOne({
			where: {
				userId: req.user.id,
				id: req.params.id,
			},
			raw: true,
		});
		if (!file) {
			res.status(400).send();
			return;
		}
		res.download(`dist/${req.user.id}/${file?.name}`);
	}
}

module.exports = FilesService;
