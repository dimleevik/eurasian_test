const bcrypt = require("bcrypt");
var jwt = require("jsonwebtoken");

const UserModel = require("../../models/users/userModel");
const JwtService = require("../jwt/jwtService");

class AuthService {
	static async login(userData, res) {
		const user = await UserModel.findOne({
			where: { email: userData.email },
			raw: true,
		});
		const isValid = await bcrypt.compare(
			userData.password,
			user?.password || ""
		);
		if (isValid) {
			res.send({
				access_token: JwtService.sign({ ...user, password: "" }),
			});
			return;
		}
		res.status(400).send();
	}

	static async register(userData, res) {
		const isExist = await UserModel.findOne({
			where: { email: userData.email },
			raw: true,
		});
		if (isExist) {
			res.status(400).send();
			return;
		}
		const user = await UserModel.create(
			{
				email: userData.email,
				password: userData.password,
			},
			{ raw: true }
		);
		res.send({ ...user, password: "" });
	}
}

module.exports = AuthService;
