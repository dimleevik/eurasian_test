const JwtService = require("../../services/jwt/jwtService");

function isAuthorized(req, res, next) {
	const [type, token] = req.headers.authorization.split(" ");
	if (type !== "Bearer") {
		req.status(400).send();
		return;
	}
	req.user = JwtService.verify(token);
	next();
}

module.exports = { isAuthorized };
